package ir.niksun.caraservice.activities.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.Gallery.adapter.RecyclerGalleryAdapter;
import ir.niksun.caraservice.databinding.ActivityGalleryBinding;

public class GalleryActivity extends AppCompatActivity {

    private ActivityGalleryBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gallery);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_gallery, null);
        setupImageRecycler();
    }

    private void setupImageRecycler() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        RecyclerGalleryAdapter adapter = new RecyclerGalleryAdapter(this);

        bind.recyclerView.setHasFixedSize(true);
        bind.recyclerView.setLayoutManager(layoutManager);
        bind.recyclerView.setAdapter(adapter);
    }
}