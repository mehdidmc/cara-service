package ir.niksun.caraservice.activities.Gallery.adapter;

import android.content.Context;
import android.content.Intent;
import android.icu.text.MessagePattern;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.Gallery.GalleryActivity;
import ir.niksun.caraservice.activities.imageSlider.ImageSliderActivity;
import ir.niksun.caraservice.databinding.GalleryImagesItemsBinding;

public class RecyclerGalleryAdapter extends RecyclerView.Adapter<RecyclerGalleryViewHolder> {

    private final Context context;
    private final int[] images;

    public RecyclerGalleryAdapter(Context context) {
        this.context = context;

        images = new int[] {
                R.drawable.image_1,
                R.drawable.image_2,
                R.drawable.image_3,
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6,
                R.drawable.image_7,
                R.drawable.image_8,
                R.drawable.image_9,
                R.drawable.image_10,
                R.drawable.image_11,
                R.drawable.image_12
        };
    }

    @NonNull
    @Override
    public RecyclerGalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        GalleryImagesItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.gallery_images_items, parent, false);
        return new RecyclerGalleryViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerGalleryViewHolder holder, int position) {
        Picasso.get().load(images[position]).into(holder.bind.imageGalleryImv);

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, ImageSliderActivity.class);
            intent.putExtra("position", position);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return images.length;
    }
}
