package ir.niksun.caraservice.activities.Gallery.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.databinding.GalleryImagesItemsBinding;

public class RecyclerGalleryViewHolder extends RecyclerView.ViewHolder {

    public GalleryImagesItemsBinding bind;

    public RecyclerGalleryViewHolder(@NonNull GalleryImagesItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
