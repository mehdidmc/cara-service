package ir.niksun.caraservice.activities.contactUs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.databinding.ActivityContactUsBinding;
import ir.niksun.caraservice.utility.ContentUtil;

public class ContactUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_contact_us);
        ActivityContactUsBinding bind = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        bind.backImv.setOnClickListener(view -> finish());
    }

    public void onClickUssdCode(View view) {
        String content = getResources().getString(R.string.ussd_code);
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(ussdToCallableUri(content));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);

        else startActivity(intent);
    }

    public Uri ussdToCallableUri(String ussd) {
        StringBuilder uriString = new StringBuilder();

        if(!ussd.startsWith("tel:"))
            uriString.append("tel:");

        for(char c : ussd.toCharArray()) {
            if(c == '#')
                uriString.append(Uri.encode("#"));
            else
                uriString.append(c);
        }
        return Uri.parse(uriString.toString());
    }
}