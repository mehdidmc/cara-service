package ir.niksun.caraservice.activities.cooler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.coolerProducts.CoolerProductsActivity;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.ActivityCoolerBinding;

public class CoolerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cooler);
        ActivityCoolerBinding bind = DataBindingUtil.setContentView(this, R.layout.activity_cooler);

        bind.back.setOnClickListener(view -> finish());

        bind.productsLayout.setOnClickListener(view -> {
            Intent intent = new Intent(this, CoolerProductsActivity.class);
            startActivity(intent);
        });

        bind.installGuidLayout.setOnClickListener(view -> {
            Intent intent = new Intent(this, ShowContentActivity.class);
            String content = "coolers/install_guide.pdf";
            intent.putExtra("content", content);
            startActivity(intent);
        });
    }
}