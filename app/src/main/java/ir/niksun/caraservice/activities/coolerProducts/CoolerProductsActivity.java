package ir.niksun.caraservice.activities.coolerProducts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.coolerProducts.adapter.CoolerProductsRecyclerAdapter;
import ir.niksun.caraservice.databinding.ActivityCoolerProductsBinding;

public class CoolerProductsActivity extends AppCompatActivity {

    private ActivityCoolerProductsBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cooler_products);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_cooler_products);
        setupProductsRecycler();

        bind.back.setOnClickListener(view-> finish());
    }

    public void setupProductsRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        CoolerProductsRecyclerAdapter adapter = new CoolerProductsRecyclerAdapter(this);

        bind.productsRecycler.setHasFixedSize(true);
        bind.productsRecycler.setLayoutManager(layoutManager);
        bind.productsRecycler.setAdapter(adapter);
    }
}