package ir.niksun.caraservice.activities.coolerProducts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.coolerProductsDetail.CoolerProductsDetailActivity;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.CoolerProductsItemsBinding;

public class CoolerProductsRecyclerAdapter extends RecyclerView.Adapter<CoolerProductsRecyclerViewHolder> {

    private final Context context;
    private final String[] items;

    public CoolerProductsRecyclerAdapter(Context context) {
        this.context = context;
        items = context.getResources().getStringArray(R.array.cooler_products);
    }

    @NonNull
    @Override
    public CoolerProductsRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        CoolerProductsItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.cooler_products_items, parent, false);
        return new CoolerProductsRecyclerViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull CoolerProductsRecyclerViewHolder holder, int position) {
        holder.bind.titleTxv.setText(items[position]);

        holder.bind.contentLayout.setOnClickListener(view -> {
            Intent intent = new Intent(context, CoolerProductsDetailActivity.class);
            String model = "";

            switch (position) {
                case 0: model = "12000";
                    break;

                case 1: model = "18000";
                    break;

                case 2: model = "24000";
                    break;

                case 3: model = "30000";
                    break;
            }

            intent.putExtra("model", model);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return items.length;
    }
}
