package ir.niksun.caraservice.activities.coolerProducts.adapter;

import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.databinding.CoolerProductsItemsBinding;

public class CoolerProductsRecyclerViewHolder extends RecyclerView.ViewHolder {

    public CoolerProductsItemsBinding bind;

    public CoolerProductsRecyclerViewHolder(CoolerProductsItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
