package ir.niksun.caraservice.activities.coolerProductsDetail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.ActivityCoolerProductsDetailBinding;

public class CoolerProductsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cooler_products_detail);
        ActivityCoolerProductsDetailBinding bind = DataBindingUtil.setContentView(this, R.layout.activity_cooler_products_detail);

        bind.back.setOnClickListener(view -> finish());

        bind.indoorProduct.setOnClickListener(view -> {
            String model = getIntent().getExtras().getString("model");
            Intent intent = new Intent(this, ShowContentActivity.class);
            String content = "";

            switch (model) {
                case "12000":
                    content = "coolers/12000/12000_indoor.pdf";
                    break;
                case "18000":
                    content = "coolers/18000/18000_indoor.pdf";
                    break;
                case "24000":
                    content = "coolers/24000/24000_indoor.pdf";
                    break;
                case "30000":
                    content = "coolers/30000/30000_indoor.pdf";
            }

            intent.putExtra("content", content);
            startActivity(intent);
        });

        bind.outdoorProduct.setOnClickListener(view -> {
            String model = getIntent().getExtras().getString("model");
            Intent intent = new Intent(this, ShowContentActivity.class);
            String content = "";

            switch (model) {
                case "12000":
                    content = "coolers/12000/12000_outdoor.pdf";
                    break;
                case "18000":
                    content = "coolers/18000/18000_outdoor.pdf";
                    break;
                case "24000":
                    content = "coolers/24000/24000_outdoor.pdf";
                    break;
                case "30000":
                    content = "coolers/30000/30000_outdoor.pdf";
            }

            intent.putExtra("content", content);
            startActivity(intent);
        });
    }
}