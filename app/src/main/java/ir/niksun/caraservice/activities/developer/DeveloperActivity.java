package ir.niksun.caraservice.activities.developer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.ActivityDeveloperBinding;

public class DeveloperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_developer);
        ActivityDeveloperBinding bind = DataBindingUtil.setContentView(this, R.layout.activity_developer);

        bind.backImv.setOnClickListener(view -> finish());

        bind.devSpeechTxv.setOnClickListener(view -> {
            Intent intent = new Intent(this, ShowContentActivity.class);
            String content = "dev_speech.pdf";
            intent.putExtra("content", content);
            startActivity(intent);
        });
    }
}