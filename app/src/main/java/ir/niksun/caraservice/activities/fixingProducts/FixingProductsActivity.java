package ir.niksun.caraservice.activities.fixingProducts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.databinding.ActivityFixingProductsBinding;

public class FixingProductsActivity extends AppCompatActivity {

    private ActivityFixingProductsBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fixing_products);

        setupLayoutBinding();
    }

    private void setupLayoutBinding() {
        bind = DataBindingUtil.setContentView(this, R.layout.activity_fixing_products);
        FixingProductsClickHandler clickHandler = new FixingProductsClickHandler(this);
        bind.setClickHandler(clickHandler);
    }
}