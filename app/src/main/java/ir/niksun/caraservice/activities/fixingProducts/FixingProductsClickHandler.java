package ir.niksun.caraservice.activities.fixingProducts;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.cooler.CoolerActivity;
import ir.niksun.caraservice.activities.productsDetail.ProductsDetailActivity;

public class FixingProductsClickHandler {

    private final Activity activity;

    public FixingProductsClickHandler(Activity activity) {
        this.activity = activity;
    }

    public void onCLickModel840(View view) {
        String name = activity.getString(R.string.refrigerator_freezer);
        String model = activity.getString(R.string.model_840);
        int color = activity.getResources().getColor(R.color.first_item_color);

        Intent detailIntent = new Intent(activity, ProductsDetailActivity.class);
        detailIntent.putExtra("model", model);
        detailIntent.putExtra("name", name);
        detailIntent.putExtra("color", color);

        activity.startActivity(detailIntent);
    }

    public void onCLickNF592(View view) {
        String name = activity.getResources().getString(R.string.freezer);
        String model = activity.getResources().getString(R.string.model_nf592);
        int color = activity.getResources().getColor(R.color.second_item_color);

        Intent detailIntent = new Intent(activity, ProductsDetailActivity.class);
        detailIntent.putExtra("model", model);
        detailIntent.putExtra("name", name);
        detailIntent.putExtra("color", color);

        activity.startActivity(detailIntent);
    }

    public void onCLickNR592(View view) {
        String name = activity.getResources().getString(R.string.refrigerator);
        String model = activity.getResources().getString(R.string.model_nr592);
        int color = activity.getResources().getColor(R.color.third_item_color);

        Intent detailIntent = new Intent(activity, ProductsDetailActivity.class);
        detailIntent.putExtra("model", model);
        detailIntent.putExtra("name", name);
        detailIntent.putExtra("color", color);

        activity.startActivity(detailIntent);
    }

    public void onClickNC702700(View view) {
        String name = activity.getResources().getString(R.string.refrigerator_freezer);
        String model = activity.getResources().getString(R.string.model_702_nc700_nc702);
        int color = activity.getResources().getColor(R.color.fifth_item_color);

        Intent detailIntent = new Intent(activity, ProductsDetailActivity.class);
        detailIntent.putExtra("model", model);
        detailIntent.putExtra("name", name);
        detailIntent.putExtra("color", color);

        activity.startActivity(detailIntent);
    }

    public void onClickCooler(View view) {
        Intent intent = new Intent(activity, CoolerActivity.class);
        activity.startActivity(intent);
    }

    public void onCLickBack(View view) {
        activity.finish();
    }
}
