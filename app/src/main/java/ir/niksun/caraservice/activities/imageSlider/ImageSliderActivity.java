package ir.niksun.caraservice.activities.imageSlider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.WindowManager;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.imageSlider.adapter.ImageSliderAdapter;
import ir.niksun.caraservice.databinding.ActivityImageSliderBinding;

public class ImageSliderActivity extends AppCompatActivity {

    private ActivityImageSliderBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_image_slider);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_image_slider, null);
        setupImageSlider();
    }

    private void setupImageSlider() {
        int position = getIntent().getExtras().getInt("position");
        ImageSliderAdapter adapter = new ImageSliderAdapter(this);
        bind.sliderView.setSliderAdapter(adapter);
        bind.sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        bind.sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        bind.sliderView.setCurrentPagePosition(position);
    }
}