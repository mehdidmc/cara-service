package ir.niksun.caraservice.activities.imageSlider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.databinding.ImageSliderItemsBinding;

public class ImageSliderAdapter extends SliderViewAdapter<ImageSliderViewHolder> {

    private final Context context;
    private final int[] images;

    public ImageSliderAdapter(Context context) {
        this.context = context;

        images = new int[] {
                R.drawable.image_1,
                R.drawable.image_2,
                R.drawable.image_3,
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6,
                R.drawable.image_7,
                R.drawable.image_8,
                R.drawable.image_9,
                R.drawable.image_10,
                R.drawable.image_11,
                R.drawable.image_12
        };
    }

    @Override
    public ImageSliderViewHolder onCreateViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ImageSliderItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.image_slider_items, parent, false);
        return new ImageSliderViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(ImageSliderViewHolder viewHolder, int position) {
        Picasso.get().load(images[position]).into(viewHolder.bind.slideImage);
    }

    @Override
    public int getCount() {
        return images.length;
    }
}
