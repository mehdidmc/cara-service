package ir.niksun.caraservice.activities.imageSlider.adapter;

import com.smarteist.autoimageslider.SliderViewAdapter;

import ir.niksun.caraservice.databinding.ImageSliderItemsBinding;

public class ImageSliderViewHolder extends SliderViewAdapter.ViewHolder {

    public ImageSliderItemsBinding bind;

    public ImageSliderViewHolder(ImageSliderItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
