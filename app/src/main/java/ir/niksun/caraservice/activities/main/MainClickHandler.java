package ir.niksun.caraservice.activities.main;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import ir.niksun.caraservice.activities.Gallery.GalleryActivity;
import ir.niksun.caraservice.activities.contactUs.ContactUsActivity;
import ir.niksun.caraservice.activities.developer.DeveloperActivity;
import ir.niksun.caraservice.activities.fixingProducts.FixingProductsActivity;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.activities.website.WebsiteActivity;

public class MainClickHandler {

    private final Activity activity;

    public MainClickHandler(Activity activity) {
        this.activity = activity;
    }

    public void onClickFixingTutorial(View view) {
        Intent fixingIntent = new Intent(activity, FixingProductsActivity.class);
        activity.startActivity(fixingIntent);
    }

    public void onClickAboutUs(View view) {
        Intent intent = new Intent(activity, ShowContentActivity.class);
        String content = "about_us.pdf";
        intent.putExtra("content", content);
        activity.startActivity(intent);
    }

    public void onCLickContactUs(View view) {
        Intent intent = new Intent(activity, ContactUsActivity.class);
        activity.startActivity(intent);
    }

    public void onCLickGallery(View view) {
        Intent intent = new Intent(activity, GalleryActivity.class);
        activity.startActivity(intent);
    }

    public void onDeveloperClicks(View view) {
        Intent intent = new Intent(activity, DeveloperActivity.class);
        activity.startActivity(intent);
    }

    public void onCLickWebsite(View view) {
        Intent intent = new Intent(activity, WebsiteActivity.class);
        activity.startActivity(intent);
    }
}
