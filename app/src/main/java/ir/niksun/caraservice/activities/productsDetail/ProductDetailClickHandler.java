package ir.niksun.caraservice.activities.productsDetail;

import android.app.Activity;
import android.view.View;

public class ProductDetailClickHandler {

    private final Activity activity;

    public ProductDetailClickHandler(Activity activity) {
        this.activity = activity;
    }

    public void onCLickBack(View view) {
        activity.finish();
    }
}
