package ir.niksun.caraservice.activities.productsDetail;

import android.app.Activity;
import android.os.Bundle;

public class ProductDetailPresenter {

    private final Activity activity;

    private String name;
    private String model;
    private int color;

    public ProductDetailPresenter(Activity activity) {
        this.activity = activity;
    }

    public void getExtras() {
        Bundle data = activity.getIntent().getExtras();
        name = data.getString("name");
        model = data.getString("model");
        color = data.getInt("color");
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public int getColor() {
        return color;
    }
}
