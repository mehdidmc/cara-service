package ir.niksun.caraservice.activities.productsDetail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.WindowManager;

import com.google.android.material.tabs.TabLayoutMediator;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.productsDetail.adapter.ProductsTabs2StateAdapter;
import ir.niksun.caraservice.activities.productsDetail.adapter.ProductsTabsStateAdapter;
import ir.niksun.caraservice.databinding.ActivityProductsDetailBinding;

public class ProductsDetailActivity extends AppCompatActivity {

    private ActivityProductsDetailBinding bind;
    private ProductDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_products_detail);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_products_detail);
        presenter = new ProductDetailPresenter(this);

        ProductDetailClickHandler clickHandler = new ProductDetailClickHandler(this);
        bind.setClickHandler(clickHandler);

        presenter.getExtras();
        setupTabsAndToolbar();
    }

    private void setupTabsAndToolbar() {
        String model = presenter.getModel();

        if (model.equals("مدل های NC700,NC702")) {
            ProductsTabs2StateAdapter tabsAdapter = new ProductsTabs2StateAdapter(this, model);
            bind.viewPager.setAdapter(tabsAdapter);

        } else {
            ProductsTabsStateAdapter tabsAdapter = new ProductsTabsStateAdapter(this, presenter.getModel());
            bind.viewPager.setAdapter(tabsAdapter);
        }

        new TabLayoutMediator(bind.tabLayout, bind.viewPager, (tab, position) -> {
            String[] tabsTitle = getResources().getStringArray(R.array.detail_tabs_title);
            tab.setText(tabsTitle[position]);
        }).attach();

        String productName = presenter.getName() + " " + presenter.getModel();
        bind.productNameTxv.setText(productName);

        bind.appBar.setBackgroundColor(presenter.getColor());
        bind.tabLayout.setBackgroundColor(presenter.getColor());
    }
}