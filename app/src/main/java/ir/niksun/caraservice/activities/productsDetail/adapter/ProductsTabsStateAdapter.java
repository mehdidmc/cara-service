package ir.niksun.caraservice.activities.productsDetail.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import ir.niksun.caraservice.activities.productsDetail.fragments.combinationsKey.CombinationsKeyFragment;
import ir.niksun.caraservice.activities.productsDetail.fragments.errors.ErrorsFragment;
import ir.niksun.caraservice.activities.productsDetail.fragments.parts.PartsFragment;

public class ProductsTabsStateAdapter extends FragmentStateAdapter {

    private final String model;

    public ProductsTabsStateAdapter(@NonNull FragmentActivity fragmentActivity, String model) {
        super(fragmentActivity);
        this.model = model;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 2: return new PartsFragment(model);
            case 1: return new CombinationsKeyFragment(model);
            default: return new ErrorsFragment(model);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
