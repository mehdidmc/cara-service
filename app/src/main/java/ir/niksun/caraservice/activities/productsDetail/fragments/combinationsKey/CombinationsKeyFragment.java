package ir.niksun.caraservice.activities.productsDetail.fragments.combinationsKey;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.productsDetail.fragments.combinationsKey.adapter.RecyclerKeyAdapter;
import ir.niksun.caraservice.databinding.FragmentCombinationsKeyBinding;

public class CombinationsKeyFragment extends Fragment {

    private String model;

    public CombinationsKeyFragment() {
        // Required empty public constructor
    }

    public CombinationsKeyFragment(String model) {
        this.model = model;
    }

    private FragmentCombinationsKeyBinding bind;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_combinations_key, container, false);
        setupRecyclerView();
        return bind.getRoot();
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerKeyAdapter adapter = new RecyclerKeyAdapter(Objects.requireNonNull(getActivity()), model);

        bind.recyclerView.setHasFixedSize(true);
        bind.recyclerView.setLayoutManager(layoutManager);
        bind.recyclerView.setAdapter(adapter);
    }
}