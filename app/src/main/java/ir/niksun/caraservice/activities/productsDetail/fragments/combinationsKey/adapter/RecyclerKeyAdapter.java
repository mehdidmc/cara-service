package ir.niksun.caraservice.activities.productsDetail.fragments.combinationsKey.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.utility.ContentUtil;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.CombinationsKeyItemsBinding;

public class RecyclerKeyAdapter extends RecyclerView.Adapter<RecyclerKeyViewHolder> {

    private final Activity activity;
    private String[] items = {};
    private final String model;

    public RecyclerKeyAdapter(Activity activity, String model) {
        this.activity = activity;
        this.model = model;

        switch (model) {
            case "مدل RF840":
                items = activity.getResources().getStringArray(R.array.rf840_keys);
                break;
            case "مدل NF592":
                items = activity.getResources().getStringArray(R.array.nf592_keys);
                break;
            case "مدل NR592":
                items = activity.getResources().getStringArray(R.array.nr592_keys);
                break;
            case "مدل های NC700,NC702" :
                items = activity.getResources().getStringArray(R.array.nc702700_keys);
                break;
        }
    }

    @NonNull
    @Override
    public RecyclerKeyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        CombinationsKeyItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.combinations_key_items, parent, false);
        return new RecyclerKeyViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerKeyViewHolder holder, int position) {
        String title = items[position];
        holder.bind.setTitle(title);

        holder.bind.contentLayout.setOnClickListener(view -> {
            Intent contentIntent = new Intent(activity, ShowContentActivity.class);
            String content = "";

            switch (model) {
                case "مدل RF840":
                    content = ContentUtil.getRF840Keys(position);
                    break;
                case "مدل NF592":
                    content = ContentUtil.getNF592Keys(position);
                    break;
                case "مدل NR592":
                    content = ContentUtil.getNR592Keys(position);
                    break;
                case "مدل های NC700,NC702" :
                    content = ContentUtil.getNC702700Keys(position);
                    break;

            }
            contentIntent.putExtra("content", content);
            activity.startActivity(contentIntent);
        });
    }

    @Override
    public int getItemCount() {
        return items.length;
    }
}
