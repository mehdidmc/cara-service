package ir.niksun.caraservice.activities.productsDetail.fragments.combinationsKey.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.databinding.CombinationsKeyItemsBinding;

public class RecyclerKeyViewHolder extends RecyclerView.ViewHolder {

    public CombinationsKeyItemsBinding bind;

    public RecyclerKeyViewHolder(@NonNull CombinationsKeyItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
