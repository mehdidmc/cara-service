package ir.niksun.caraservice.activities.productsDetail.fragments.errors;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Objects;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.productsDetail.fragments.errors.adapter.ErrorsItemAdapter;
import ir.niksun.caraservice.databinding.FragmentErrorsBinding;

public class ErrorsFragment extends Fragment {

    private String model;

    public ErrorsFragment() {
        // Required empty public constructor
    }

    public ErrorsFragment(String model) {
        this.model = model;
    }

    private FragmentErrorsBinding bind;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_errors, container, false);
        setupRecyclerView();
        return bind.getRoot();
    }

    private void setupRecyclerView() {
        ErrorsItemAdapter adapter = new ErrorsItemAdapter(Objects.requireNonNull(getContext()), model);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        bind.recyclerView.setHasFixedSize(true);
        bind.recyclerView.setLayoutManager(layoutManager);
        bind.recyclerView.setAdapter(adapter);
    }
}