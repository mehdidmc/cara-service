package ir.niksun.caraservice.activities.productsDetail.fragments.errors.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.ErrorsItemViewBinding;
import ir.niksun.caraservice.utility.ContentUtil;

public class ErrorsItemAdapter extends RecyclerView.Adapter<ErrorsItemViewHolder> {

    private final Context context;
    private String[] items = {};
    private final String model ;

    public ErrorsItemAdapter(Context context, String model) {
        this.context = context;
        this.model = model;

        switch (model) {
            case "مدل RF840":
                items = context.getResources().getStringArray(R.array.rf840_errors);
                break;
            case "مدل NF592":
                items = context.getResources().getStringArray(R.array.nf592_errors);
                break;
            case "مدل NR592":
                items = context.getResources().getStringArray(R.array.nr592_errors);
                break;
            case "مدل های NC700,NC702" :
                items = context.getResources().getStringArray(R.array.nc702700_errors);
                break;
        }
    }

    @NonNull
    @Override
    public ErrorsItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ErrorsItemViewBinding bind = DataBindingUtil.inflate(inflater, R.layout.errors_item_view, parent, false);
        return new ErrorsItemViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull ErrorsItemViewHolder holder, int position) {
        String title = items[position];
        holder.bind.warningTitleTxv.setText(title);

        holder.bind.contentLayout.setOnClickListener(v -> {
            Intent contentIntent = new Intent(context, ShowContentActivity.class);
            String content = "";

            switch (model) {
                case "مدل RF840":
                    content = ContentUtil.getRF840Errors(position);
                    break;
                case "مدل NF592":
                    content = ContentUtil.getNF592Errors(position);
                    break;
                case "مدل NR592":
                    content = ContentUtil.getNR592Errors(position);
                    break;
                case "مدل های NC700,NC702" :
                    content = ContentUtil.getNC702700Errors(position);
                    break;
            }

            contentIntent.putExtra("content", content);
            context.startActivity(contentIntent);
        });
    }

    @Override
    public int getItemCount() {
        return items.length;
    }
}
