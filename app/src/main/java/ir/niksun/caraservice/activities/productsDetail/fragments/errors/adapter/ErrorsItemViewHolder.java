package ir.niksun.caraservice.activities.productsDetail.fragments.errors.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.databinding.ErrorsItemViewBinding;

public class ErrorsItemViewHolder extends RecyclerView.ViewHolder {

    public ErrorsItemViewBinding bind;

    public ErrorsItemViewHolder(@NonNull ErrorsItemViewBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
