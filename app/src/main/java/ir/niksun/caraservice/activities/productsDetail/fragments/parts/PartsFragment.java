package ir.niksun.caraservice.activities.productsDetail.fragments.parts;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.productsDetail.fragments.parts.adapter.RecyclerPartItemAdapter;
import ir.niksun.caraservice.databinding.FragmentPartsBinding;

public class PartsFragment extends Fragment {

    private String model;

    public PartsFragment() {
        // Required empty public constructor
    }

    public PartsFragment(String model) {
        this.model = model;
    }

    private FragmentPartsBinding bind;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_parts, container, false);
        setupRecyclerView();
        return bind.getRoot();
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerPartItemAdapter adapter = new RecyclerPartItemAdapter(getContext(), model);

        bind.recyclerView.setHasFixedSize(true);
        bind.recyclerView.setLayoutManager(layoutManager);
        bind.recyclerView.setAdapter(adapter);
    }
}