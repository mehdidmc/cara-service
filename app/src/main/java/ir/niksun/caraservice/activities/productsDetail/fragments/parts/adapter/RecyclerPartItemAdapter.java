package ir.niksun.caraservice.activities.productsDetail.fragments.parts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.showContent.ShowContentActivity;
import ir.niksun.caraservice.databinding.PartsItemBinding;
import ir.niksun.caraservice.utility.ContentUtil;

public class RecyclerPartItemAdapter extends RecyclerView.Adapter<RecyclerPartItemViewHolder> {

    private final Context context;
    private final String model;
    private final String[] items;

    public RecyclerPartItemAdapter(Context context, String model) {
        this.context = context;
        this.model = model;

        if (model.equals("مدل RF840"))
            items = context.getResources().getStringArray(R.array.rf840_parts);

        else
            items = context.getResources().getStringArray(R.array.nr592_nf592_parts);
    }

    @NonNull
    @Override
    public RecyclerPartItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        PartsItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.parts_item, parent, false);
        return new RecyclerPartItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerPartItemViewHolder holder, int position) {
        String title = items[position];
        holder.bind.setTitle(title);

        holder.bind.contentLayout.setOnClickListener(view -> {
            Intent contentIntent = new Intent(context, ShowContentActivity.class);
            String content = "";

            if (model.equals("مدل RF840"))
                content = ContentUtil.getRF840Parts();

            else if (model.equals("مدل NF592") || model.equals("مدل NR592"))
                content = ContentUtil.getNF592NR592Parts();

            contentIntent.putExtra("content", content);
            context.startActivity(contentIntent);
        });
    }

    @Override
    public int getItemCount() {
        if (items.length == 0) return 0;
        else return items.length;
    }
}
