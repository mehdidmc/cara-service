package ir.niksun.caraservice.activities.productsDetail.fragments.parts.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.niksun.caraservice.databinding.PartsItemBinding;

public class RecyclerPartItemViewHolder extends RecyclerView.ViewHolder {

    public PartsItemBinding bind;

    public RecyclerPartItemViewHolder(@NonNull PartsItemBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
