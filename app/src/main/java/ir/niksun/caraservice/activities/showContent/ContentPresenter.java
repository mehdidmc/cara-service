package ir.niksun.caraservice.activities.showContent;

import android.app.Activity;
import android.os.Bundle;

public class ContentPresenter {

    private final Activity activity;
    private final ContentView view;

    private String content;

    public ContentPresenter(Activity activity, ContentView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getExtras() {
        Bundle data = activity.getIntent().getExtras();
        content = data.getString("content");
        view.setPDFContent(content);
    }
}
