package ir.niksun.caraservice.activities.showContent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.WindowManager;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.databinding.ActivityShowContentBinding;

public class ShowContentActivity extends AppCompatActivity implements ContentView {

    private ActivityShowContentBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_show_content);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_show_content);

        ContentPresenter presenter = new ContentPresenter(this, this);
        presenter.getExtras();
    }

    @Override
    public void setPDFContent(String path) {
        bind.pdfView
                .fromAsset(path)
                .enableSwipe(true)
                .enableDoubletap(true)
                .enableAntialiasing(true)
                .spacing(0)
                .load();
    }
}