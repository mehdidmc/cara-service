package ir.niksun.caraservice.activities.splash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.activities.main.MainActivity;
import ir.niksun.caraservice.databinding.ActivitySplashBinding;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ActivitySplashBinding bind = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_anim);
        bind.logo.setAnimation(animation);

        new Handler().postDelayed(() -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }, 3500);
    }
}