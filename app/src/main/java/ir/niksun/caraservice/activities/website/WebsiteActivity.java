package ir.niksun.caraservice.activities.website;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ir.niksun.caraservice.R;
import ir.niksun.caraservice.databinding.ActivityWebsiteBinding;
import ir.niksun.caraservice.utility.ContentUtil;

public class WebsiteActivity extends AppCompatActivity {

    private ActivityWebsiteBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_website);
        setupWebsite();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebsite() {
        WebViewClient client = new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                bind.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                bind.progressBar.setVisibility(View.INVISIBLE);
            }
        };

        bind.webView.setWebViewClient(client);
        bind.webView.getSettings().getLoadsImagesAutomatically();
        bind.webView.getSettings().setJavaScriptEnabled(true);
        bind.webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        bind.webView.loadUrl(ContentUtil.WEB_URL);
    }
}