package ir.niksun.caraservice.utility;

public class ContentUtil {

    public static final String WEB_URL = "http://caraservice.ir/";

    public static String getRF840Errors(int position) {
        switch (position) {
            case 0: return "rf840/errors/01_840_F1.pdf";
            case 1: return "rf840/errors/01_840_F2.pdf";
            case 2: return "rf840/errors/01_840_F3.pdf";
            case 3: return "rf840/errors/01_840_F4.pdf";
            case 4: return "rf840/errors/02_840_E1.pdf";
            case 5: return "rf840/errors/02_840_E2.pdf";
            case 6: return "rf840/errors/02_840_E3.pdf";
            case 7: return "rf840/errors/03_840_H1.pdf";
            case 8: return "rf840/errors/03_840_H2.pdf";
            case 9: return "rf840/errors/04_840_C1.pdf";
            case 10: return "rf840/errors/04_840_C2.pdf";
            default: return "rf840/errors/05_840_L1.pdf";
        }
    }

    public static String getNF592Errors(int position) {
        switch (position) {
            case 0: return "nf592/errors/01_nf592_E1.pdf";
            case 1: return "nf592/errors/01_nf592_E2.pdf";
            case 2: return "nf592/errors/02_nf592_H1.pdf";
            case 3: return "nf592/errors/03_nf592_C1.pdf";
            default:return "nf592/errors/03_nf592_C2.pdf";
        }
    }

    public static String getNR592Errors(int position) {
        switch (position) {
            case 0: return "nr592/errors/01_nr592_e2.pdf";
            case 1: return "nr592/errors/01_nr592_e3.pdf";
            case 2: return "nr592/errors/02_nr592_h1.pdf";
            case 3: return "nr592/errors/03_nr592_c1.pdf";
            default:return "nr592/errors/03_nr592_c2.pdf";
        }
    }

    public static String getNC702700Errors(int position) {
        switch (position) {
            case 0: return "nc702700/errors/01_nc702700_e1.pdf";
            case 1: return "nc702700/errors/01_nc702700_e2.pdf";
            case 2: return "nc702700/errors/02_nc702700_h1.pdf";
            case 3: return "nc702700/errors/02_nc702700_h2.pdf";
            case 4: return "nc702700/errors/03_nc702700_c1.pdf";
            default:return "nc702700/errors/03_nc702700_c2.pdf";
        }
    }

    public static String getRF840Keys(int position) {
        switch (position) {
            case 0: return "rf840/keys/01_rf840_monitor_classic.pdf";
            case 1: return "rf840/keys/02_rf840_monitor_sc.pdf";
            case 2: return "rf840/keys/03_rf840_monitor_horizontal.pdf";
            case 3: return "rf840/keys/04_rf840_monitor_sn.pdf";
            case 4: return "rf840/keys/05_rf840_monitor_sg.pdf";
            default:return "rf840/keys/06_rf840_monitor_sd.pdf";
        }
    }

    public static String getNF592Keys(int position) {
        switch (position) {
            case 0: return "nf592/keys/01_nf592_monitor_classic.pdf";
            case 1: return "nf592/keys/02_nf592_monitor_horizontal.pdf";
            case 2: return "nf592/keys/03_nf592_monitor_sn.pdf";
            case 3: return "nf592/keys/04_nf592_monitor_sg.pdf";
            default:return "nf592/keys/05_nf592_monitor_ss.pdf";
        }
    }

    public static String getNR592Keys(int position) {
        switch (position) {
            case 0: return "nr592/keys/01_nr592_monitor_classic.pdf";
            case 1: return "nr592/keys/02_nr592_monitor_horizontal.pdf";
            case 2: return "nr592/keys/03_nr592_monitor_sn.pdf";
            case 3: return "nr592/keys/04_nr592_monitor_sg.pdf";
            default:return "nr592/keys/05_nr592_monitor_ss.pdf";
        }
    }

    public static String getNC702700Keys(int position) {
        switch (position) {
            case 0: return "nc702700/keys/01_nc702700_monitor_classic.pdf";
            case 1: return "nc702700/keys/02_nc702700_monitor_sc.pdf";
            case 2: return "nc702700/keys/03_nc702700_monitor_horizontal.pdf";
            case 3: return "nc702700/keys/04_nc702700_monitor_sn.pdf";
            case 4: return "nc702700/keys/05_nc702700_monitor_sg.pdf";
            default:return "nc702700/keys/06_nc702700_monitor_sd.pdf";
        }
    }

    public static String getRF840Parts() {
        return "rf840/manual/service_manual_840.pdf";
    }

    public static String getNF592NR592Parts() {
        return "nf592/manual/service_manual_twins.pdf";
    }
}
